# Ubuntu Touch device tree for Unihertz Titan

This is based on Halium 10.0, and uses the mechanism described in [this
page](https://github.com/ubports/porting-notes/wiki/GitLab-CI-builds-for-devices-based-on-halium_arm64-(Halium-9)).

**Unihertz are GPL violators** and have been [ignoring kernel source code
requests](https://www.reddit.com/r/UnihertzTitan/comments/i6m311/unihertz_are_gpl_violators_no_kernel_source_code/)
[sent](https://twitter.com/mariogrip/status/1346851913074171904) by multiple
people. Coincidentally, this heavily limits the possibilities for
alternative OS projects for this device. Current Ubuntu Touch adaptation in
this repo is based on prebuilt stock Android 10 kernel, binary-patched to
allow unsigned kernel module loading, and a bunch of workarounds/hacks to
get around kernel feature requirements for certain components, such as
missing namespaces support for Halium LXC container.

In particular, AppArmor LSM module [has been modified](https://gitlab.com/ubports/community-ports/android10/unihertz-titan/kernel-alps-mt6771/-/commits/halium-10.0/security/apparmor)
to compile as loadable kernel module by resolving private kernel symbols
at initialization time. This allows to compile it in Linux kernel source tree
used as base for MT6771 chipset devices, and load it with stock kernel.
However, since LSMs are not designed to function as loadable modules, it
required quite a number of hacks and may crash the kernel if module is loaded
too late, so it's insmoded before running upstart init.

This project can be built manually (see the instructions below) or you can
download the ready-made artifacts from gitlab: take the [latest
archive](https://gitlab.com/ubports/community-ports/android10/unihertz-titan/unihertz-titan/-/jobs/artifacts/master/download?job=devel-flashable),
unpack the `artifacts.zip` file (make sure that all files are created inside a
directory called `out/`, then follow the instructions in the
[Install](#install) section.


# Status
* **Based on prebuilt stock kernel** (see above)
* Working:
  - Basic cellular connectivity
  - Sound
  - Wi-Fi
  - Bluetooth
  - Rear/front cameras for taking photos
* Known issues:
  - Making more than a single voice call per boot (will be fixed [once libhybris is updated](https://gitlab.com/ubports/core/packaging/libhybris/-/merge_requests/31))
  - Video recording with cameras
  - Fingerprint sensor not functional

Please make a issue for things that do not work that *aren't* on this list.


## How to build

To manually build this project, follow these steps:

```bash
./build.sh -b workdir  # workdir is the name of the build directory
./build/prepare-fake-ota.sh out/device_Titan.tar.xz ota
./build/system-image-from-ota.sh ota/ubuntu_command out
```


## Install

First you need to update your device to Android 10 stock firmware and install
[TWRP build](https://unihertz-titan.neocities.org/).

Download the [latest
artifacts archive](https://gitlab.com/ubports/community-ports/android10/unihertz-titan/unihertz-titan/-/jobs/artifacts/master/download?job=devel-flashable)
of devel-flashable CI job or build manually. After doing that, run

```bash
fastboot flash boot out/boot.img
fastboot reboot recovery
```

In TWRP, format the userdata partition to ext4 to remove encryption and ensure
it is mounted, then run

```bash
adb push out/ubuntu.img /data
adb reboot
```
